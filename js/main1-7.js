var app = new Vue({
        el: '#app',

        data: {
            tasks: [
                { description: 'Go to the store', completed: false },
                { description: 'Finish him', completed: false },
                { description: 'Make a move', completed: false },
                { description: 'Clear room', completed: false },
                { description: 'Make dinner', completed: false },
                { description: 'Play with her', completed: false },
            ],
            newTask: ""
        },


        computed: {
            completedTasks() {
                return this.tasks.filter(task => task.completed);
            },

            incompletedTasks() {
                return this.tasks.filter(task => ! task.completed);
            }
        },


        methods: {
            addNewTask() {
                let value = this.newTask;
                if (!value) {
                    return;
                };
                this.tasks.push({
                    description: value,
                    completed: false
                });
                this.newTask = '';
            }
        }
    });